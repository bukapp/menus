import mongoose from "mongoose";

const mongoHost = 'mongodb://root:' + process.env.MONGO_PASSWORD + '@' + process.env.MONGO_HOST + ':27017/';

export default function
    createConnection(){
        mongoose
            .connect(mongoHost)
            .then(() => {
                console.log('connection established');
            })
            .catch((error) => {
                console.log('connection error: ' + error);
                process.exit(1);
            });
    }
